document.querySelectorAll(".tabs-nav div").forEach( div => {
    div.addEventListener("click", event => {
        document.querySelector('.is-active').classList.remove('is-active');
        event.target.classList.add("is-active");
        chooseTab(event.target.dataset.tabname)
    })
})
const chooseTab = (tabMark) => {
    document.querySelector('.isActive').classList.remove('isActive');
    document.querySelector(`.tab.${tabMark}`).classList.add("isActive");
}